module.exports = {
  name: 'node-app',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/node-app'
};
