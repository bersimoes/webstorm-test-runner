# BuggedTestRunner

## Issue

Webstorm 2018.2 doesn't pickup tsconfig file on NX project app folder. This causes webstorm to:

- be unable to run angular or node app tests.
- confuse karma with jest specs.

This issue has been reported on [NX github](https://github.com/nrwl/nx/issues/816) but seems to be an editor issue, afecting VSCode as well as webstorm.

## How to reproduce:

- open project in webstorm
- got to `src/app/app.component.spec.ts`, click on 'Run AppComponent', test fails to run with: `Jest encountered an unexpected token`

## Expected behaviour:

Tests should run like if they where run with the CLI with:

- ng test angular-app

## Repository creation steps:

0. Install [NX and angular CLI](https://nrwl.io/nx/guide-getting-started)
1. create-nx-workspace bugged-test-runner
2. ng g app angular-app
3. ng g jest
4. ng g node-app node-app
6. create node test

## Webstorm versions affected:

- #WS-182.4505.50
- #WS-183.3795.15
 
